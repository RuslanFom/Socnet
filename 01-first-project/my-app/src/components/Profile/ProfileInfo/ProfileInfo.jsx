import React from 'react';
import s from './ProfileInfo.module.css';
import Preloader from "../../common/Preloader/Preloader";
import ProfileStatus from "./ProfileStatus"

const ProfileInfo = (props) => {
    if (!props.profile) {                   /* "!" - это отрицание, если "props.profile отсутствует"*/
        return <Preloader />                 /* то возвращаем крутилку */
    }
    return (
        <div>
           {/* <div>
                <img
                    src="https://helpx.adobe.com/content/dam/help/en/photoshop/using/convert-color-image-black-white/jcr_content/main-pars/before_and_after/image-before/Landscape-Color.jpg"
                    alt=""/>
            </div>*/}
            <div className={s.descriptionBlock}>
                <img src={props.profile.photos.large} />
                <ProfileStatus status={"У меня полчается!!!"}/>
            </div>
        </div>
    )
}

export default ProfileInfo;
import React from "react";
import Loader from "../../../assets/images/Loader.svg";

let Preloader = (props) => {
    return <div><img src={Loader}/></div>
}

export default Preloader;